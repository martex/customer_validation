<?php
class Validation_Customer_Adminhtml_CustomerMassActionController extends Mage_Adminhtml_Controller_Action
{

    public function massDisableAction()
    {
        $customersId = $this->getRequest()->getParam('customer');
        foreach ($customersId as $customerId){
        $customer = Mage::getModel('customer/customer')->load($customerId);
            $customer->setData('customer_validation_it' , '0');

            try {
                $customer->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }

        }

        $this->_redirect('adminhtml/customer');

    }

    public function massEnableAction()
    {
        $customersId = $this->getRequest()->getParam('customer');
        foreach ($customersId as $customerId){
            $customer = Mage::getModel('customer/customer')->load($customerId);
            $customer->setData('customer_validation_it' , '1');

            try {
                $customer->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }

            $body = 'Your account was validated';
            $mail = Mage::getModel('core/email');
            $mail->setToName($customer->getFirstname().' '.$customer->getLastname());
            $mail->setToEmail($customer->getEmail());
            $mail->setBody($body);
            $mail->setSubject('Validated letter');
            $mail->setFromEmail('yourstore@url.com');
            $mail->setFromName("Magento test");
            $mail->setType('text');// You can use 'html' or 'text'

            try {
                $mail->send();
                Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
            }

        }

        $this->_redirect('adminhtml/customer');

    }

}