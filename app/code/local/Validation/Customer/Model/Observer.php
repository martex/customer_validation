<?php
class Validation_Customer_Model_Observer
{
    public function customerSave($event)
    {

        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }

            $customer = $event->getCustomer();
            $body = 'Please wait for your account to be activated';
            $mail = Mage::getModel('core/email');
            $mail->setToName($customer->getFirstname().' '.$customer->getLastname());
            $mail->setToEmail($customer->getEmail());
            $mail->setBody($body);
            $mail->setSubject('Greeting letter');
            $mail->setFromEmail('yourstore@url.com');
            $mail->setFromName("Magento test");
            $mail->setType('text');// You can use 'html' or 'text'

            try {
                $mail->send();
                Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                //$this->_redirect('');
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
                //$this->_redirect('');
            }

    }

    public function beforeBlockToHtml(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $grid = $observer->getBlock();

        /**
         * Mage_Adminhtml_Block_Customer_Grid
         */
        if ($grid instanceof Mage_Adminhtml_Block_Customer_Grid) {
            $grid->addColumnAfter(
                'customer_validation_it',
                array(
                    'header' => Mage::helper('Validation_Customer')->__('Customer Validation'),
                    'index'  => 'customer_validation_it',
                    'type'    => 'options',
                    'options' => array('0' => 'Disabled', '1' => 'Enabled')
                ),
                'entity_id'
            );
        }
    }

    public function beforeCollectionLoad(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $collection = $observer->getCollection();
        if (!isset($collection)) {
            return;
        }

        /**
         * Mage_Customer_Model_Resource_Customer_Collection
         */
        if ($collection instanceof Mage_Customer_Model_Resource_Customer_Collection) {
            /* @var $collection Mage_Customer_Model_Resource_Customer_Collection */
            $collection->addAttributeToSelect('customer_validation_it');
        }
    }
    public function addField(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $block = $observer->getBlock();
        $customer = Mage::registry('current_customer');
        if (!isset($block)) return;

        switch ($block->getType()) {
            case 'adminhtml/customer_edit_tab_account':

                $form = $block->getForm();
               $fieldset = $form->getElement('base_fieldset');
                $fieldset->removeField('customer_validation_it');
                if ($fieldset) {

                    $fieldset->addField('customer_validation_it', 'select', array(
                        'label'     => Mage::helper('Validation_Customer')->__('Customer Validation'),
                        'required'  => true,
                        'name'      => 'customer_validation_it',
                        'onclick' => "",
                        'onchange' => "",
                        'value'  => $customer->getData('customer_validation_it'),
                        'values' => array('0' => 'Disable','1' => 'Enable'),
                        'disabled' => false,
                        'readonly' => false,
                        'after_element_html' => '<small>Comments</small>',
                        'tabindex' => 1
                    ));
                }

                break;
        }
    }
    public  function beforeSave(Varien_Event_Observer $observer)
    {

        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $data = Mage::app()->getRequest()->getParam('account');
        if ($data['customer_validation_it']){
            $customer = $observer->getCustomer();
            $customer-> setData('customer_validation_it', $data['customer_validation_it']);
            if ($data['customer_validation_it'] == 1){

                $body = 'Your account was validated';
                $mail = Mage::getModel('core/email');
                $mail->setToName($customer->getFirstname().' '.$customer->getLastname());
                $mail->setToEmail($customer->getEmail());
                $mail->setBody($body);
                $mail->setSubject('Validated letter');
                $mail->setFromEmail('yourstore@url.com');
                $mail->setFromName("Magento test");
                $mail->setType('text');// You can use 'html' or 'text'

                try {
                    $mail->send();
                    Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                } catch (Exception $e) {
                    Mage::getSingleton('core/session')->addError('Unable to send.');
                }
            }
        }


    }

    public function addMassAction(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }
        $block = $observer->getEvent()->getBlock();
        if(get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() == 'customer')
        {
            $block->addItem('mass_disabled', array(
                'label' => 'Mass Disable',
                'url' => Mage::app()->getStore()->getUrl('adminhtml/CustomerMassAction/massDisable'),
            ));
            $block->addItem('mass_enabled', array(
                'label' => 'Mass Enable',
                'url' => Mage::app()->getStore()->getUrl('adminhtml/CustomerMassAction/massEnable'),
            ));
        }
    }

    public function customerLogin(Varien_Event_Observer $observer)
    {


        $helper = Mage::helper('Validation_Customer');
        if (!$helper->isModuleEnabled()) {
            return;
        }

        $customer = $observer->getEvent()->getCustomer();
        $session = Mage::getSingleton('customer/session');

        if ($customer->getCustomerValidationIt() == 0) {
            /*
             * Fake the old logout() method without deleting the session and all messages
             */
            $session->setCustomer(Mage::getModel('customer/customer'))
                ->setId(null)
                ->setCustomerGroupId(Mage_Customer_Model_Group::NOT_LOGGED_IN_ID);

                /*
                 * If this is a regular registration, simply display message
                 */
                $message = $helper->__('Please wait for your account to be activated');

                $session->addSuccess($message);

        }
    }



}