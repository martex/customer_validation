
<?php
$installer = $this;
$installer->startSetup();

$setup = Mage::getModel('customer/entity_setup', 'core_setup');

$setup->addAttribute("customer", "customer_validation_it",  array(
    "type"     => "int",
    "backend"  => "",
    "label"    => "Customer Validation",
    "input"    => "select",
    "source"   => "",
    "visible"  => true,
    "required" => true,
    "default" => '0',
    "frontend" => "",
    "unique"     => false,
    'user_defined' => 1,
));

$installer->endSetup();
