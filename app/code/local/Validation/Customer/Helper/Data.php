<?php
class Validation_Customer_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isModuleEnabled()
    {
       return  Mage::getStoreConfig('customer/customer_configuration/enabled');
    }

}